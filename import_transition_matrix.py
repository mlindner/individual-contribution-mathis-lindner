import os 
import pandas as pd
'''
takes string (name = 'matrix.csv')
return DF
'''

def get_transition_matrix(name):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    transition_matrices_folder_path = dir_path.replace(r'tools','transition_matrices')
    os.chdir(transition_matrices_folder_path)
    matrix = pd.read_csv(name)
    return matrix


'''
takes string (name = 'matrix.csv')
return matrix
'''
def get_transition_matrix_2nd(name):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    transition_matrices_folder_path = dir_path.replace(r'tools','transition_matrices')
    os.chdir(transition_matrices_folder_path)
    matrix = pd.read_csv(name, index_col=[0,1], skipinitialspace=True) #this part does not provide the correct index
    return matrix

