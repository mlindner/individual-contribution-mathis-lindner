from typing import Sequence
from numpy.core.numeric import full_like
import numpy as np
import pandas as pd
import csv
from import_transition_matrix import get_transition_matrix

'''
matrix_name as string: 'file_name.csv'
n: how many sequences
k: length of sequences

returns generated sequences
'''

def generate_sequences_markov(matrix_name, n = 1, k = 4):
    transition_matrix = get_transition_matrix(matrix_name)
    transition_matrix = transition_matrix.drop(columns = ['0'])
    #renorm the probabilities of the rows
    transition_matrix = transition_matrix.div(transition_matrix.sum(axis=1), axis=0)
    markov_space = transition_matrix.columns.to_list()
    sequences = []
    for _ in range (n):
        sequence = []
        current_state = 'N' #or random ?
        next_state = ''
        for _ in range (k + 1):
            if (current_state == '0'):
                break
            probabilities = transition_matrix.loc[current_state].to_list()
            next_state = np.random.choice(a = markov_space,p = probabilities)
            sequence.append(current_state)
            current_state = next_state
        sequence.pop(0)
        sequences.append(sequence)
    return sequences


