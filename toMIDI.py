
import os
import EasyMIDI as EM
import numpy as np
from random import randint
from conversion_dictionary import empty #eympty is jsut a silent chord
chords = chorddict = np.load('final_chord_dict.npy', allow_pickle = 'TRUE').item()


def goto_generated_midi():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    output_path = dir_path.replace('tools','generated_midi')
    os.chdir(output_path)
"""
function that takes an array of chords that creates a midi file
chords: list of chords
filename: name of the exported midi file
chordlength: how long the chord should be played for(in seconds), default is 2 seconds
"""

def list_to_midi(chord_sequence, filename = 'default', mode = 'only_chords'):
    if (mode == 'only_chords'): 
        song = get_only_chords_song(chord_sequence)
    elif mode == 'get_mode1':
        song = get_mode1_song(chord_sequence)
    #####add other modes here
    goto_generated_midi()
    song.writeMIDI(filename + '.mid')


def elevate_octave_of_notes(notes_in_chord):
    for note in notes_in_chord:
        current_octave = note.getOctave()
        if (current_octave < 7):
            current_octave = current_octave + 1
        note.setOctave(current_octave)
    return notes_in_chord
"""
converts sequence directly to MIDI
"""
def get_only_chords_song(chord_sequence):
    track = EM.Track('acoustic grand piano')
    for chord_string in chord_sequence:
        try:
            track.addNotes(chords[chord_string]) #if dict not complete
        except KeyError:
            error_message = "chord dictionary doesn't include this chord yet: " + str(chord_string)
            print(error_message)
    song = EM.EasyMIDI()
    song.addTrack(track)
    return song

def get_mode1_song(chord_sequence):
    cs= []
    for chord in chord_sequence:
        if chord in chords:
            cs.append(chords[chord])
        else:
            print("chords[\"" + chord + "\"] = Chord([])")
            cs.append(chords['empty_1'])
    song = EM.EasyMIDI()
    chord_track = EM.Track('acoustic bass')
    melody_track = EM.Track('acoustic grand piano')
    ###ADD CHORDS AND NOTES FROM CHORD SEQUENCE
    for current_chord in cs:
        notes_in_chord = current_chord.getNotes()
        elevated_notes = elevate_octave_of_notes(notes_in_chord)
        if current_chord == chords['empty_1']:
            empty.setDuration(1)
            melody_track.addNote(empty)
        else:
            for _ in range(4):
                random_index = randint(a = 0, b = len(elevated_notes))
                try:
                    random_note = elevated_notes[random_index]
                    random_note.setDuration(1/4)
                    melody_track.addNote(random_note)
                except:
                    empty.setDuration(1/4)
                    melody_track.addNote(empty) #add empty note
        current_chord.setDuration(1)
        chord_track.addChord(current_chord)
    song.addTrack(chord_track)
    song.addTrack(melody_track)
    return song